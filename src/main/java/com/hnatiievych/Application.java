package com.hnatiievych;


/**
 * Main class in apllication
 *
 * @since 10.11.2019
 */
public class Application {
    /**
     * Program generate numbers interval with users number;
     * prints odd numbers from start to the end of interval and even from end to start;
     * prints the sum of odd and even numbers;
     * build Fibonacci numbers: F1 will be the biggest odd number and
     * F2 – the biggest even number, user can enter the size of set (N);
     * Program prints percentage of odd and even Fibonacci numbers;
     *
     * @param args User enter from command line
     * @author Hnatiievych Yarolsav
     * @version 1.1.0
     * @since 10.11.2019
     */

    public static void main(String[] args) {
        Interval interval=new Interval();
        interval.initInterval();
        interval.printOddAndEvenNumber();
        interval.sumEvenNumber();
        interval.sumOddNumber();
        int f1=interval.findBiggestOddNumber();
        int f2=interval.findBiggestEvenNumber();
        Fibonacci fibonacci=new Fibonacci();
        fibonacci.initFibonacciSet(f1,f2);
        fibonacci.findPercentegeOddAndEvenNumber();

    }
}
