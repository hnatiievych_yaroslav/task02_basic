package com.hnatiievych;

import java.util.Scanner;


/**
 *
 * The class describes the creation of an interval
 * of numbers the limit is entered by the user.
 * @author Hnatiievych Yarolsav
 * @version 1.1.0
 * @since 10.11.2019
 */


public class Interval{

    /**
     * generated interval
     */
    private int[]interval;

    /**
     * Method build the interval, with user's parameter read from command line.
     * @return interval
     */
    public int[]initInterval(){

        Scanner scan=new Scanner(System.in).useDelimiter("\\D");
        System.out.println("Enter limit interval, like 1:100");
        int startNumber=Integer.parseInt(scan.next());
        int endNumber=Integer.parseInt(scan.next());
        this.interval=new int[endNumber-(startNumber-1)];

        for (int i=0;i<interval.length;i++){
            interval[i]=startNumber;
            startNumber++;
        }
        return interval;
    }

    /**
     * Method print odd and even number from interval
     */
    public void printOddAndEvenNumber(){
        System.out.print("Odd Number = ");
        for(int i:this.interval){
            if(i%2!=0){
                System.out.print(i + " ");
            }
        }
        System.out.println("");

        System.out.print("Even Number = ");
        for (int i=interval.length-1;i>=interval[0];i--){
            if(interval[i]%2==0){
                System.out.print(interval[i] + " ");
            }
        }
        System.out.println();
    }

    /**
     * The method prints sum of even number from the interval
     */
    public void sumEvenNumber(){
        int[]set=this.interval;
        int sum=0;
        for(int i=0;i<set.length;i++){
            if(set[i]%2==0){
                sum+=set[i];
            }
        }
        System.out.println("sum of even numbers = " + sum);
    }

    /**
     * The method prints sum of odd number from the interval
     */
    public void sumOddNumber(){
        int[]set=this.interval;
        int sum=0;
        for(int i=0;i<set.length;i++){
            if(set[i]%2!=0){
                sum+=set[i];
            }
        }
        System.out.println("sum of odd numbers = " + sum);
    }


    /**
     * The method finds the biggest odd number from the interval
     * @return the biggest odd number.
     */
    public int findBiggestOddNumber(){
        int biggerOddNumber=this.interval[0];
        for(int i:interval){
            if(i%2!=0){
                if(biggerOddNumber<i){
                    biggerOddNumber=i;
                }
            }
        }
       return biggerOddNumber;
    }

    /**
     * The method finds the biggest even number from the interval
     */
    public int findBiggestEvenNumber(){
        int biggerEvenNumber=this.interval[0];
        for(int i:interval){
            if(i%2==0){
                if(biggerEvenNumber<i){
                    biggerEvenNumber=i;
                }
            }
        }
        return biggerEvenNumber;
    }
}
