package com.hnatiievych;

import java.util.Arrays;
import java.util.Scanner;


/**
 * The class describes the creation of an Fibonacci number set
 *
 * @author Hnatiievych Yarolsav
 * @version 1.1.0
 * @since 10.11.2019
 */

public class Fibonacci{

    /**
     * fibonacci set
     */
    private int[]set;


    /**
     * This method builds fibonacci number set
     *
     * @param f1 the biggest odd number from interval
     * @param f2 the biggest even number from interval
     */
    public void initFibonacciSet(final int f1,final int f2){

        System.out.println("Enter the size of Fibonacci set");
        Scanner scan=new Scanner(System.in);
        int sizeOfSet=scan.nextInt();
        this.set=new int[sizeOfSet];
        int n1=f1;
        int n2=f2;
        this.set[0]=n1;
        this.set[1]=n2;
        for (int i=2;i<sizeOfSet;i++){
            int n3=n1+n2;
            this.set[i]=n3;
            n1=n2;
            n2=n3;
        }
        System.out.println("Fibonacci set: "+Arrays.toString(set));
    }

    /**
     * ]
     * The method finds percentage odd and even numbers from fibonacci number set
     */
    public void findPercentegeOddAndEvenNumber(){
        int countEvenNumber=0;
        int countOddNumber=0;
        for (int i:set){
            if(i!=0){
                if(i%2==0){
                    countEvenNumber++;
                }else{
                    countOddNumber++;
                }
            }
        }
        double percentEvenNumbers=countEvenNumber*100/set.length;
        double percentOddNumbers=countOddNumber*100/set.length;
        System.out.println("Percentage even Fibonacci numbers = "+percentEvenNumbers+"\n"+
                "Percentage odd Fibonacci numbers = "+percentOddNumbers);
    }
}
